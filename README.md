# Dynmod

## Description 

Dynmod is a simple herd growth model spreadsheet (under Microsoft Office Excel) for ruminant livestock populations. Originally, Dynmod was designed for pedagogical objectives. The underlying demographic model is therefore very simplified. In practice, however, it also can help non-specialists to implement fast and crude ex ante or ex post demographic diagnostics, in various applications as for example livestock population management, herd production estimation or exploration of scenarios in development projects.

Dynmod simulates the dynamics of the size of a livestock population and the number of animals produced per year. As the technical and economic results are a function of herd dynamics, Dynmod also calculates live weights, meat production and secondary productions (milk, skin and hides, manure) at population level, as well financial outputs that can be used in more integrated financial calculations (e.g. benefit-costs ratios or internal return rates). Finally, Dynmod provides crude estimates of the population feeding requirements in dry mater.

A first version of Dynmod was initially developed jointly by CIRAD and ILRI (International Livestock Research Institute) in 2007. Several new versions have then been designed by CIRAD. 

## One model, different hypotheses, 3 modules

Two types of demographic projection can be produced with Dynmod, depending on the type of demographic diagnosis the user wishes to make. 

The **Proj** simulation simulates the number of animals produced each year over a period ranging from 1 to 20 years. Simulated annual production depends deterministically on demographic parameters. Proj carries out simulations that can be used, for example, to assess the capacity of the herd to reach demographic equilibrium after a shock or the variation in production after an uncontrolled event (shock, drought).

The **steady** simulation is carried out for one year only under the assumption that the sex-age structure and the growth rate are constant over a fairly long period (generally at least 10 years). We say that we are in a stationary demographic regime (as opposed to a transitory demographic regime). In other words, we cannot, for example, consider a bad year but a bad situation that is true several years, enough years after the shock  triggered by low values of natural demographic parameters. The advantage of steady simulation is that it is easier to compare the results of simulations of different demographic situations because we know that the simulated animal production is directly correlated to the demographic parameters set by the user.

DYNMOD consists in 3 spreadsheet modules:

- [STEADY1](https://gitlab.cirad.fr/selmet/livtools/dynmod/-/blob/master/dynmod_steady1.xlsx): simulation of the 1-year population production assuming a demographic steady state – with constant sex-by-age structure and growth rate;

- [STEADY2](https://gitlab.cirad.fr/selmet/livtools/dynmod/-/blob/master/dynmod_steady2.xlsx): simulation of the 1-year population production assuming a demographic steady state – with constant adult population size and null growth rate;

- [PROJ](https://gitlab.cirad.fr/selmet/livtools/dynmod/-/blob/master/dynmod_proj.xlsm): simulation of the population dynamics and production over a period of time in a possibly variable environment (the period can last from 1 to 20 years).

## How to install
Simply download the Microsoft Excel files to get started. Note that the workbook for the PROJ demographic projection has an xlsm file extension because it contains a macro that allows you to duplicate the input parameters for up to 20 simulation years. If you have a version of Microsoft Excel that does not support macros (e.g. microsoft Excel for Mac), you can open this file as an ordinary Microsoft Excel file. You will need to manually enter the parameters for the simulation.

## Documentation
See the [manual](https://gitlab.cirad.fr/selmet/livtools/dynmod/-/blob/master/manual_dynmod_v3.pdf)

## Support
maintainer : Samir Messad (samir.messad@cirad.fr)

## Project status
No development planned
